int ports[] = {
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9
};
const unsigned int n_ports = sizeof(ports) / sizeof(ports[0]);
bool channel_state[n_ports];

enum input_state_t {
  IDLE,
  ONOFF
} input_state;

void print_channels() {
  Serial.print("channel_state: ");
  for(unsigned int i = 0; i < n_ports; i++) {
    Serial.print(i);
    Serial.print(":");
    Serial.print(channel_state[i] ? "1 " : "0 ");
  }
  Serial.println("");
}
void setup() {                
  // initialize the digital pin as an output.
  for(unsigned int i = 0; i < n_ports; i++) {
    pinMode(ports[i], OUTPUT);     
    channel_state[i] = false;
  }
  set_channels();
  Serial.begin(115200);
  delay(1000);               // wait for a second
  delay(1000);               // wait for a second
  Serial.println("hello flo! to enable channel 0 send \"0:1\\n\". to disable channel 3: \"3:0\\n\", to disable all: \"x:0\\n\"\n");  
  print_channels();  
  input_state = IDLE;
}

void set_channels() {
  for(unsigned int i = 0; i < n_ports; i++)
    digitalWrite(ports[i], channel_state[i] ? LOW : HIGH);   
  print_channels();
}

char channel[32];
unsigned int channel_idx = 0;

void serial_input() {
  int inp = Serial.read();
  switch(input_state) {
    case IDLE:
      if(isdigit(inp)) {
        if(channel_idx < sizeof(channel) - 1)
          channel[channel_idx++] = inp;
      } else if(inp == 'x') {
        channel_idx = 0;
        channel[channel_idx++] = inp;
      } else if(inp == ':') {
        input_state = ONOFF;
      } else if(inp == '\n' || inp == '\r') {
        set_channels();
      }
      break;
    case ONOFF: {
      if(isdigit(inp)) {
        bool is_on = inp == '1';
        channel[channel_idx] = 0;
        channel_idx = 0;
        if(channel[0] == 'x') {
          for(unsigned int ch = 0; ch < n_ports; ch++)
            channel_state[ch] = is_on;
        } else {
          unsigned int ch = atoi(channel);
          if(ch < n_ports)
            channel_state[ch] = is_on;
        }
      }
      input_state = IDLE;
      break;
    }
  }
}

void loop() {
  if(Serial.available() > 0)
    serial_input();
}
